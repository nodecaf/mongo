
const originalMongo = require('mongodb');

module.exports = async function(conf, collections = {}){
    conf = conf || {};
    conf.options = conf.options || {};

    let client = new originalMongo.MongoClient(conf.url, { useUnifiedTopology: true, ...conf.options });
    await client.connect();
    let mongo = {
        ...originalMongo,
        db: client.db(conf.db),
        client,
        id(id){
            try{ return new originalMongo.ObjectID(id) }
            catch(err) { return false }
        },
        close(){
            return client.close();
        }
    };

    for(let key in collections)
        mongo[key] = mongo.db.collection(collections[key]);

    return mongo;
}
