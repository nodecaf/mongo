const assert = require('assert');

const mongo = require('../lib/main');

const MONGO_HOST = process.env.MONGO_HOST || 'localhost:27017';

describe('Nodecaf Mongo', () => {

    it('Should create a mongo client', async function(){
        let mym = await mongo({ url: 'mongodb://' + MONGO_HOST, db: 'foo_test' }, {
            foo: 'Foo'
        });
        let data = { bar: 'bar' };
        await mym.foo.insertOne(data);
        assert(await mym.foo.findOne({ _id: mym.id(data._id) }));
        await mym.foo.drop();
        await mym.close();
    });

});
